package com.unicentr.fitstat.ui.adapters;

import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;

import com.unicentr.fitstat.model.FitDevice;

/**
 * Created by NewUser on 26.08.16.
 */

public class AllDeviceStorages extends DevicesStorage {
    private RecyclerView recyclerView;
    private SortedList<FitDevice> sortedList;

    private AllDeviceStorages(){}

    public AllDeviceStorages(final RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        this.sortedList = new SortedList<>(FitDevice.class, new SortedListAdapterCallback<FitDevice>(recyclerView.getAdapter()) {
            @Override
            public int compare(FitDevice o1, FitDevice o2) {
                return o1.getAddress().compareTo(o2.getAddress());
            }

            @Override
            public boolean areContentsTheSame(FitDevice oldItem, FitDevice newItem) {
                return oldItem.getAddress().equals(newItem.getAddress());
            }

            @Override
            public boolean areItemsTheSame(FitDevice item1, FitDevice item2) {
                return item1.getAddress().equals(item2.getAddress());
            }

            @Override
            public void onInserted(int position, int count) {
                recyclerView.getAdapter().notifyItemRangeInserted(position, count);
            }

            @Override
            public void onChanged(int position, int count) {
                super.onChanged(position, count);
            }
        });
    }

    @Override
    public int getCount() {
        return sortedList.size();
    }

    @Override
    public FitDevice getDeviceAt(int index) {
        return sortedList.get(index);
    }

    @Override
    public boolean addDevice(FitDevice fitDevice) {
        sortedList.add(fitDevice);
        return true;
    }

    @Override
    public boolean removeDevice(FitDevice fitDevice) {
        return sortedList.remove(fitDevice);
    }

    @Override
    public boolean removeDeviceAt(int index) {
        return sortedList.removeItemAt(index)!=null;
    }
}
