package com.unicentr.fitstat.ui.fragments;

import android.Manifest;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.unicentr.fitstat.R;
import com.unicentr.fitstat.common.BaseFragment;
import com.unicentr.fitstat.model.FitDevice;
import com.unicentr.fitstat.ui.adapters.AllDeviceStorages;
import com.unicentr.fitstat.ui.adapters.MemoryDeviceAdapter;
import com.unicentr.fitstat.ui.adapters.SQLDeviceStorage;
import com.zhaoxiaodan.miband.ActionCallback;
import com.zhaoxiaodan.miband.MiBand;

import butterknife.BindView;

/**
 * Created by NewUser on 25.08.16.
 */

public class EditFragment extends BaseFragment {
    @BindView(R.id.recyclerActiveDevices)
    protected RecyclerView recyclerActiveDevices;
    @BindView(R.id.recyclerStorageDevices)
    protected RecyclerView recyclerStorageDevice;

    protected AllDeviceStorages allDeviceStorages;
    protected MemoryDeviceAdapter memoryDeviceAdapter;

    protected MemoryDeviceAdapter storageAdapter;

    protected ScanCallback scanCallback;

    @Override
    public int getLayout() {
        return R.layout.fragment_edit;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initScanCallback();
        allDeviceStorages = new AllDeviceStorages(recyclerActiveDevices);
        memoryDeviceAdapter = new MemoryDeviceAdapter(allDeviceStorages);
        recyclerActiveDevices.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerActiveDevices.setHasFixedSize(true);
        recyclerActiveDevices.setAdapter(memoryDeviceAdapter);

        SQLDeviceStorage sqlDeviceStorage = new SQLDeviceStorage(getContext());
        storageAdapter = new MemoryDeviceAdapter(sqlDeviceStorage);
        recyclerStorageDevice.setHasFixedSize(true);
        recyclerStorageDevice.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerStorageDevice.setAdapter(storageAdapter);
    }

    private void initScanCallback() {
        scanCallback = new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                BluetoothDevice device = result.getDevice();
                Log.d(TAG,
                        "找到附近的蓝牙设备: name:" + device.getName() + ",uuid:"
                                + device.getUuids() + ",add:"
                                + device.getAddress() + ",type:"
                                + device.getType() + ",bondState:"
                                + device.getBondState() + ",rssi:" + result.getRssi());
                FitDevice fitDevice = new FitDevice();
                fitDevice.setName(device.getName());
                fitDevice.setAddress(device.getAddress());
                memoryDeviceAdapter.addItem(fitDevice);
               /* String item = device.getName() + "|" + device.getAddress();
                if (!devices.containsKey(item)) {
                    devices.put(item, device);
                    adapter.add(item);
                }*/

            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        MiBand.startScan(scanCallback);
    }

    @Override
    public void onStop() {
        super.onStop();
        MiBand.stopScan(scanCallback);
    }
}
