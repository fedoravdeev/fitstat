package com.unicentr.fitstat.ui.adapters;

import com.unicentr.fitstat.model.FitDevice;

/**
 * Created by NewUser on 26.08.16.
 */

public abstract class DevicesStorage {
    abstract public int getCount();
    abstract public FitDevice getDeviceAt(int index);
    abstract public boolean addDevice(FitDevice fitDevice);
    abstract public boolean removeDevice(FitDevice fitDevice);
    abstract public boolean removeDeviceAt(int index);
}