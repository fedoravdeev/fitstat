package com.unicentr.fitstat.ui.fragments;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.Button;

import com.unicentr.fitstat.R;
import com.unicentr.fitstat.common.BaseFragment;
import com.unicentr.fitstat.model.Constants;
import com.unicentr.fitstat.service.ParseService;
import com.unicentr.fitstat.ui.activity.MainActivity;

import net.ralphpina.permissionsmanager.PermissionsManager;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by MacUser on 25.08.16.
 */

public class MainFragment extends BaseFragment{

    @BindView(R.id.btnStart)
    protected Button btnStart;

    @Override
    public int getLayout() {
        return R.layout.fragment_main;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initServiceStatus();
    }

    private void initServiceStatus() {
        if (ParseService.IS_SERVICE_RUNNING) {
            btnStart.setText("Stop Parse Devices");
        } else {
            btnStart.setText("Start Parse Devices");
        }
    }

    @OnClick(R.id.btnEditDevices)
    protected void clickEdit(){
        if (PermissionsManager.get()
                .hasAskedForLocationPermission()) {
            getAct().addFragment(getAct(), EditFragment.class, R.id.fragment_container, null, true, true);
        } else {
            PermissionsManager.get()
                    .requestLocationPermission(this);
        }
    }

    @OnClick(R.id.btnStart)
    protected void clickStart(){
        Intent service = new Intent(getAct(), ParseService.class);
        if (!ParseService.IS_SERVICE_RUNNING) {
            service.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
            ParseService.IS_SERVICE_RUNNING=true;
        } else {
            service.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
            ParseService.IS_SERVICE_RUNNING=false;
        }
        getAct().startService(service);
        initServiceStatus();
    }

}
