package com.unicentr.fitstat.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.unicentr.fitstat.R;
import com.unicentr.fitstat.model.FitDevice;

/**
 * Created by NewUser on 26.08.16.
 */

public abstract class DevicesAdapter extends RecyclerView.Adapter<DevicesAdapter.ViewHolder> implements View.OnClickListener {
    private DevicesStorage devicesStorage;

    private DevicesAdapter() {}

    protected static int viewResource = R.drawable.ic_add_green_500_24dp;

    public DevicesAdapter(DevicesStorage devicesStorage) {
        this.devicesStorage = devicesStorage;
    }

    @Override
    public void onClick(View view) {
        FitDevice fitDevice = devicesStorage.getDeviceAt(Integer.parseInt(view.getTag().toString()));
        eventBtn(fitDevice, view.getContext());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        protected TextView tvName;
        protected ImageButton btnAdd;

        public ViewHolder(View v) {
            super(v);
            tvName = (TextView) v.findViewById(R.id.tvName);
            btnAdd = (ImageButton) v.findViewById(R.id.btnAdd);
            btnAdd.setImageResource(viewResource);
        }
    }

    abstract void eventBtn(FitDevice index, Context context);

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_device, parent, false);
        // set the view's size, margins, paddings and layout parameters
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FitDevice fitDevice = devicesStorage.getDeviceAt(position);
        holder.tvName.setText(fitDevice.getName());
        holder.btnAdd.setOnClickListener(this);
        holder.btnAdd.setTag(""+position);
    }

    @Override
    public int getItemCount() {
        return devicesStorage.getCount();
    }

    public void addItem(FitDevice fitDevice){
        devicesStorage.addDevice(fitDevice);
    }
}
