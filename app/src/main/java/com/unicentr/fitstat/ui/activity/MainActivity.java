package com.unicentr.fitstat.ui.activity;

import android.os.Bundle;

import com.unicentr.fitstat.R;
import com.unicentr.fitstat.common.BaseActivity;
import com.unicentr.fitstat.ui.fragments.MainFragment;

import net.ralphpina.permissionsmanager.PermissionsManager;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addFragment(this, MainFragment.class, R.id.fragment_container, null, false, false);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

}
