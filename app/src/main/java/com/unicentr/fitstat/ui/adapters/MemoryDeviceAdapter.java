package com.unicentr.fitstat.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.unicentr.fitstat.R;
import com.unicentr.fitstat.database.DBHelper;
import com.unicentr.fitstat.database.DBQueryManager;
import com.unicentr.fitstat.model.FitDevice;

/**
 * Created by NewUser on 26.08.16.
 */

public class MemoryDeviceAdapter extends DevicesAdapter {

    private static final String TAG = "MemoryAdapter";

    public MemoryDeviceAdapter(DevicesStorage devicesStorage) {
        super(devicesStorage);
        viewResource = R.drawable.ic_add_green_500_24dp;

    }

    @Override
    void eventBtn(FitDevice fitDevice, Context context) {
        Log.d(TAG, "Click: "+ fitDevice);
        DBHelper dbHelper = new DBHelper(context.getApplicationContext());
        boolean add = dbHelper.addFitDevice(fitDevice);
        String text = add ? "Device added!" : "Error: device already added!" ;
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
        dbHelper = null;
    }

}
