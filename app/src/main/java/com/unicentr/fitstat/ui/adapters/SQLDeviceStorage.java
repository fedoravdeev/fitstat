package com.unicentr.fitstat.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.unicentr.fitstat.database.DBHelper;
import com.unicentr.fitstat.database.DBQueryManager;
import com.unicentr.fitstat.model.FitDevice;

/**
 * Created by NewUser on 26.08.16.
 */

public class SQLDeviceStorage extends DevicesStorage {
    private DBHelper dbHelper;
    private DBQueryManager dbQueryManager;

    public SQLDeviceStorage(Context context) {
        dbHelper = new DBHelper(context);
        dbQueryManager = new DBQueryManager(dbHelper.getWritableDatabase());
    }

    @Override
    public int getCount() {
        return dbQueryManager.getCountFitDevice().size();
    }

    @Override
    public FitDevice getDeviceAt(int index) {
        return dbQueryManager.getCountFitDevice().get(index);
    }

    @Override
    public boolean addDevice(FitDevice fitDevice) {
        dbHelper.addFitDevice(fitDevice);
        return false;
    }

    @Override
    public boolean removeDevice(FitDevice fitDevice) {
        dbHelper.removeFitDevice(fitDevice.getAddress());
        return false;
    }

    @Override
    public boolean removeDeviceAt(int index) {
        try {
            dbQueryManager.getCountFitDevice().remove(index);
            return true;
        }catch (Exception e){
            Log.d("TAG",e.toString()+": - do not remove fitness device...");
            return false;
        }

    }
}
