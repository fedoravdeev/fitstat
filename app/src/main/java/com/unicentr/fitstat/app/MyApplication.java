package com.unicentr.fitstat.app;

import android.app.Application;

import net.ralphpina.permissionsmanager.PermissionsManager;

/**
 * Created by NewUser on 26.08.16.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        PermissionsManager.init(this);
    }
}
