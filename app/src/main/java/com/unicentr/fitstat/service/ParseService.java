package com.unicentr.fitstat.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.unicentr.fitstat.R;
import com.unicentr.fitstat.database.DBHelper;
import com.unicentr.fitstat.database.DBQueryManager;
import com.unicentr.fitstat.model.Constants;
import com.unicentr.fitstat.model.FitDevice;
import com.unicentr.fitstat.ui.activity.MainActivity;
import com.zhaoxiaodan.miband.ActionCallback;
import com.zhaoxiaodan.miband.MiBand;
import com.zhaoxiaodan.miband.listeners.HeartRateNotifyListener;
import com.zhaoxiaodan.miband.listeners.NotifyListener;
import com.zhaoxiaodan.miband.model.VibrationMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by NewUser on 26.08.16.
 */

public class ParseService extends Service {
    public static final String TAG = "ParseService";
    public static boolean IS_SERVICE_RUNNING = false;

    protected ScanCallback scanCallback;
    protected HashMap<BluetoothDevice, Long> hashDevices;
    protected HashMap<BluetoothDevice, Long> hashConnectedDevices;

    private MiBand miBand;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction().equals(Constants.ACTION.STARTFOREGROUND_ACTION)) {
            IS_SERVICE_RUNNING = true;
            Log.i(TAG, "Received Start Foreground Intent ");
            showNotification();
            Toast.makeText(this, "Service Started!", Toast.LENGTH_SHORT).show();
            startAsync();
        } else if (intent.getAction().equals(Constants.ACTION.STOPFOREGROUND_ACTION)) {
            stopForeground(true);
            stopSelf();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void startAsync() {
        miBand = new MiBand(getApplicationContext());
        startScanCallback();
        new ParseAsync().execute(1);
    }

    private void showNotification() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setTicker("Service working")
                .setContentText("Ok")
                .setSmallIcon(R.mipmap.ic_launcher)
                //.setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .build();
        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE,
                notification);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service Stopped!", Toast.LENGTH_SHORT).show();
        IS_SERVICE_RUNNING = false;
        if (scanCallback != null) {
            MiBand.stopScan(scanCallback);
        }
    }

    class ParseAsync extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            DBHelper dbHelper = new DBHelper(getApplicationContext());
            hashConnectedDevices = new HashMap<>();
            int i = 0;
            while (ParseService.IS_SERVICE_RUNNING) {
                ArrayList<FitDevice> fitDevices = new ArrayList<>(
                        new DBQueryManager(dbHelper.getReadableDatabase()).getCountFitDevice());

                for (final Map.Entry<BluetoothDevice, Long> entry : hashDevices.entrySet()) {
                    for (final FitDevice fitDevice : fitDevices) {
                        if (!IS_SERVICE_RUNNING) break;
                        if (fitDevice.getAddress().equals(entry.getKey().getAddress())) {
                            if (hashConnectedDevices.containsKey(entry.getKey())) continue;
                            Log.d(TAG, "Start connect to: "+entry.getKey().getName());
                            updateNotificationConnect(entry.getKey().getName());
                            final BluetoothDevice bluetoothDevice = entry.getKey();

                            miBand.connect(bluetoothDevice, new ActionCallback() {
                                @Override
                                public void onSuccess(Object data) {
                                    hashConnectedDevices.put(bluetoothDevice, System.currentTimeMillis());
                                    miBand.setDisconnectedListener(new NotifyListener() {
                                        @Override
                                        public void onNotify(byte[] data) {
                                            Log.d(TAG, "Disconnect listener - remove "+bluetoothDevice.getName());
                                            hashConnectedDevices.remove(bluetoothDevice);
                                        }
                                    });
                                    Log.d(TAG, "Connected: "+bluetoothDevice.getName());
                                    miBand.setHeartRateScanListener(new HeartRateNotifyListener() {
                                        @Override
                                        public void onNotify(int heartRate) {
                                            Log.d(TAG, "heart rate: " + heartRate);
                                            miBand.startVibration(VibrationMode.VIBRATION_WITH_LED);

                                        }
                                    });
                                    miBand.startHeartRateScan();
                                    miBand.setNormalNotifyListener(new NotifyListener() {
                                        @Override
                                        public void onNotify(byte[] data) {
                                            String tmp = "";
                                            for (int i = 0; i < data.length; i++) {
                                                tmp+=data[i];
                                            }
                                            Log.d(TAG, "Data: "+tmp);
                                        }
                                    });
                                }

                                @Override
                                public void onFail(int errorCode, String msg) {
                                    Log.d(TAG, "Fail: " + errorCode + " " + msg + " 连接断开!!!");
                                }
                            });
                            try {
                                Thread.sleep(5000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.d(ParseService.TAG, "Iteration: " + i++);

            }

            return null;
        }
    }

    private void startScanCallback() {
        hashDevices = new HashMap<>();
        scanCallback = new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                BluetoothDevice device = result.getDevice();


                if (!hashDevices.containsKey(device)) {
                    hashDevices.put(device, System.currentTimeMillis());
                    Log.d(TAG, "Add callback");
                    Log.d(TAG,
                            "找到附近的蓝牙设备: name:" + device.getName() + ",uuid:"
                                    + device.getUuids() + ",add:"
                                    + device.getAddress() + ",type:"
                                    + device.getType() + ",bondState:"
                                    + device.getBondState() + ",rssi:" + result.getRssi());
                }
               /* String item = device.getName() + "|" + device.getAddress();
                if (!devices.containsKey(item)) {
                    devices.put(item, device);
                    adapter.add(item);
                }*/

            }
        };
        MiBand.startScan(scanCallback);
    }

    private void updateNotificationConnect(String name){
        NotificationManagerCompat mNotificationManager =
                NotificationManagerCompat.from(getApplicationContext());
// Sets an ID for the notification, so it can be updated
        NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(this)
                .setContentTitle("New Connect")
                .setContentText("Connected: "+name)
                .setSmallIcon(R.mipmap.ic_launcher);
        //mNotifyBuilder.setContentText()
        //        .setNumber(++numMessages);
        // Because the ID remains unchanged, the existing notification is
        // updated.
        mNotificationManager.notify(
                Constants.NOTIFICATION_ID.FOREGROUND_SERVICE,
                mNotifyBuilder.build());

    }
}
