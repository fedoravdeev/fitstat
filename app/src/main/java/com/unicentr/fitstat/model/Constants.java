package com.unicentr.fitstat.model;

/**
 * Created by NewUser on 26.08.16.
 */

public class Constants {
    public static class ACTION {
        public static String STARTFOREGROUND_ACTION = "com.unicentr.fitstat.model.start";
        public static String STOPFOREGROUND_ACTION = "com.unicentr.fitstat.model.stop";
    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
    }
}