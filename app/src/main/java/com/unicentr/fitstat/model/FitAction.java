package com.unicentr.fitstat.model;

/**
 * Created by MacUser on 26.08.16.
 */

public class FitAction {
    private String nameAction;
    private int id;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getNameAction() {
        return nameAction;
    }

    public void setNameAction(String nameAction) {
        this.nameAction = nameAction;
    }
}
