package com.unicentr.fitstat.model;

/**
 * Created by NewUser on 26.08.16.
 */

public class FitDevice {
    private String name;

    public FitDevice(int id, String name,  String address) {
        this.name = name;
        this.id = id;
        this.address = address;
    }

    public FitDevice() {
        this.name = "No Device";
        this.address = "00:00:00:00:00:00";
    }

    public int getId() {
        return id;
    }

/*    public void setId(int id) {
        this.id = id;
    }*/

    private int id;
    private String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "FitDevice{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", address='" + address + '\'' +
                '}';
    }
}
