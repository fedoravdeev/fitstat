package com.unicentr.fitstat.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.unicentr.fitstat.R;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment {
    protected String TAG = ""+getClass().getName();
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public abstract int getLayout();

    public BaseActivity getAct(){
        return (BaseActivity) getActivity();
    }

    //public abstract void cancel();

    public static Fragment getFragmentByTag(AppCompatActivity activity, String tag){
        Fragment fragment = activity.getSupportFragmentManager()
                .findFragmentByTag(tag);
        return fragment;
    }

    public static<T> Fragment addFragment(BaseFragment baseFragment
            , Class<T> clazz
            , @Nullable Bundle args
            , boolean backstack){
        Fragment fragment = baseFragment.getChildFragmentManager().findFragmentByTag(clazz.getCanonicalName());
        if (fragment==null) {
            try {
                Constructor<?> cons = clazz.getConstructor();
                fragment = (Fragment) cons.newInstance();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (java.lang.InstantiationException e) {
                e.printStackTrace();
            }
        }

        if (fragment==null) return null;

        FragmentTransaction transaction = baseFragment.getChildFragmentManager()
                .beginTransaction();

        if (backstack) {
            transaction.addToBackStack(clazz.getCanonicalName());
        }
        transaction
                /*.setCustomAnimations(R.anim.slide_in_up, R.anim.stay
                        , R.anim.stay, R.anim.slide_out_down)*/
                .addToBackStack(null)
                .add(R.id.fragment_container, fragment, fragment.getClass().getCanonicalName())
                .commitAllowingStateLoss();
        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
