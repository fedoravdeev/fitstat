package com.unicentr.fitstat.common;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;



/**
 * Created by NewUser on 24.06.16.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        //ButterKnife.bind(this);
    }

    public abstract int getLayout();

    @Override
    public void onBackPressed() {
        if (!returnBackStackImmediate(getSupportFragmentManager())) {
            super.onBackPressed();
        }
    }

    // HACK: propagate back button press to child fragments.
    // This might not work properly when you have multiple fragments adding multiple children to the backstack.
    // (in our case, only one child fragments adds fragments to the backstack, so we're fine with this)
    private boolean returnBackStackImmediate(FragmentManager fm) {
        List<Fragment> fragments = fm.getFragments();
        if (fragments != null && fragments.size() > 0) {
            for (Fragment fragment : fragments) {
                if (fragment==null) continue;
                if (fragment.getChildFragmentManager().getBackStackEntryCount() > 0) {
                    if (fragment.getChildFragmentManager().popBackStackImmediate()) {
                        Log.d("Base: ", "fragment.getChildFragmentManager().popBackStackImmediate()");
                        return true;
                    } else {
                        Log.d("Base: ", "!ragment.getChildFragmentManager().popBackStackImmediate()");
                        return returnBackStackImmediate(fragment.getChildFragmentManager());
                    }
                }
            }
        }
        return false;
    }

    public static<T> Fragment addFragment(AppCompatActivity activity
            , Class<T> clazz
            , int container
            , @Nullable Bundle args
            , boolean backstack
            , boolean replace){
        Fragment fragment = activity.getSupportFragmentManager().findFragmentByTag(clazz.getCanonicalName());
        if (fragment==null) {
            try {
                Constructor<?> cons = clazz.getConstructor();
                fragment = (Fragment) cons.newInstance();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (java.lang.InstantiationException e) {
                e.printStackTrace();
            }
        }

        if (fragment==null) return null;

        if (args!=null) {
            fragment.setArguments(args);
        }

        FragmentTransaction transaction = activity.getSupportFragmentManager()
                .beginTransaction();
        if (backstack) {
            transaction.addToBackStack(clazz.getCanonicalName());
        }
        /*transaction
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left
                        , R.anim.enter_from_left, R.anim.exit_to_right);*/
        if (replace) {
            transaction.replace(container, fragment, fragment.getClass().getCanonicalName());
        } else {
            transaction.add(container, fragment, fragment.getClass().getCanonicalName());
        }
        transaction.commitAllowingStateLoss();
        return fragment;
    }

    public static<T> Fragment addFragment(AppCompatActivity activity
            , Class<T> clazz
            , int container
            , @Nullable Bundle args
            , boolean backstack){
        Fragment fragment = activity.getSupportFragmentManager().findFragmentByTag(clazz.getCanonicalName());
        if (fragment==null) {
            try {
                Constructor<?> cons = clazz.getConstructor();
                fragment = (Fragment) cons.newInstance();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (java.lang.InstantiationException e) {
                e.printStackTrace();
            }
        }

        if (fragment==null) return null;

        if (args!=null) {
            fragment.setArguments(args);
        }

        FragmentTransaction transaction = activity.getSupportFragmentManager()
                .beginTransaction();
        if (backstack) {
            transaction.addToBackStack(clazz.getCanonicalName());
        }
        /*transaction
                .setCustomAnimations(R.anim.slide_in_up, R.anim.stay
                        , R.anim.stay, R.anim.slide_out_down);*/
       /* if (replace) {
            transaction.replace(container, fragment, fragment.getClass().getCanonicalName());
        } else {
            transaction.add(container, fragment, fragment.getClass().getCanonicalName());
        }*/
        transaction.add(container, fragment, fragment.getClass().getCanonicalName());

        transaction.commitAllowingStateLoss();

        return fragment;
    }
}
