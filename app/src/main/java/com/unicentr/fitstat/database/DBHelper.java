package com.unicentr.fitstat.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.unicentr.fitstat.model.FitAction;
import com.unicentr.fitstat.model.FitDevice;

import java.util.ArrayList;

/**
 * Created by MacUser on 25.08.16.
 */
public class DBHelper extends SQLiteOpenHelper {
    private final static String TAG = "DBHelper";

    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "fitstat_db";


    public static final String MAC_ADDRESS_TABLE = "mac_address_table";
    public static final String ACTION_TABLE = "action_table";
    public static final String EVENT_TABLE = "event_table";

    public static final String MAC_ADDRESS_ID = "mac_address_id";
    public static final String MAC_ADDRESS_MAC = "mac_address_mac";
    public static final String MAC_ADDRESS_NAME = "mac_address_name";

    public static final String ACTION_ID = "action_id";
    public static final String ACTION_NAME = "action_name";

    public static final String EVENT_MAC_ID = "event_mac_id";
    public static final String EVENT_TIME = "event_time";
    public static final String EVENT_ID_ACTION = "event_id_action";
    public static final String EVENT_VALUE = "event_value";

    public static final String SELECTION_MAC_ADDRESS = MAC_ADDRESS_MAC + " LIKE  ?";
    public static final String SELECTION_MAC_ID = EVENT_MAC_ID + " =  ?";



    private static final String CREATE_TABLE_MAC =
            " CREATE TABLE " + MAC_ADDRESS_TABLE + " (" +
                    MAC_ADDRESS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    MAC_ADDRESS_MAC + " TEXT NOT NULL, " +
                    MAC_ADDRESS_NAME + " TEXT NOT NULL ) ; ";

    private static final String CREATE_TABLE_ACTIONS =
            " CREATE TABLE " + ACTION_TABLE + " (" +
                    ACTION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    ACTION_NAME + " TEXT NOT NULL ) ; ";

    private static final String CREATE_TABLE_EVENTS =
            " CREATE TABLE " + EVENT_TABLE + " (" +
                    EVENT_MAC_ID + " INTEGER , " +
                    EVENT_TIME + " LONG , " +
                    EVENT_ID_ACTION + " INTEGER, " +
                    EVENT_VALUE + " TEXT NOT NULL ) ; ";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating required tables
        db.execSQL(CREATE_TABLE_MAC);
        db.execSQL(CREATE_TABLE_ACTIONS);
        db.execSQL(CREATE_TABLE_EVENTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + MAC_ADDRESS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + ACTION_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + EVENT_TABLE);
        // create new tables
        onCreate(db);
    }

    public boolean addFitDevice(FitDevice fitDevice){
        DBQueryManager dbQueryManager = new DBQueryManager(this.getReadableDatabase());
        ArrayList<FitDevice> fitDevices = new ArrayList<>(dbQueryManager.getCountFitDevice());
        Log.d(TAG, "Length: "+fitDevices.size());
        for (FitDevice device : fitDevices) {
            Log.d(TAG, "Device: "+device+" Fitdevice: "+fitDevice);
            if (fitDevice.getAddress().equals(device.getAddress())) {
                Log.e(TAG, "This device is already added!");
                return false;
            }
        }

        ContentValues newValues = new ContentValues();
        newValues.put(MAC_ADDRESS_MAC, fitDevice.getAddress());
        newValues.put(MAC_ADDRESS_NAME, fitDevice.getName());

        getWritableDatabase().insert(MAC_ADDRESS_TABLE, null, newValues);
        Log.d(TAG, "Device added!");
        return true;
    }

    public void removeFitDevice(String macAddress){
        getWritableDatabase().delete(MAC_ADDRESS_TABLE, SELECTION_MAC_ADDRESS, new String[]{macAddress});
    }


    public void addFitAction(FitAction fitAction){
        ContentValues newValues = new ContentValues();
        newValues.put(ACTION_ID, fitAction.getId());
        newValues.put(ACTION_NAME, fitAction.getNameAction());

        getWritableDatabase().insert(ACTION_TABLE, null, newValues);

    }

    public void addNewEvent(FitDevice fitDevice, long timeStamp, FitAction fitAction, String value){
        ContentValues newValues = new ContentValues();
        newValues.put(EVENT_MAC_ID, fitDevice.getId());
        newValues.put(EVENT_TIME, timeStamp);
        newValues.put(EVENT_ID_ACTION, fitAction.getId());
        newValues.put(EVENT_VALUE, value);

        getWritableDatabase().insert(EVENT_TABLE, null, newValues);

    }

    public void removeEventByMac(String macAddress){
        getWritableDatabase().delete(EVENT_MAC_ID, SELECTION_MAC_ID, new String[]{macAddress});
    }

    public void removeEventPeriod(long startTime, long finishTime){
        // TODO

    }
    public void removeEventByAction(int idAction){
        // TODO

    }
    public void removeEventAll(){
        // TODO

    }
}
