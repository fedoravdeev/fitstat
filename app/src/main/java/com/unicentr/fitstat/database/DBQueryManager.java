package com.unicentr.fitstat.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.unicentr.fitstat.model.FitDevice;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MacUser on 26.08.16.
 */

public class DBQueryManager {
    private SQLiteDatabase database;

    public DBQueryManager(SQLiteDatabase database) {
        this.database = database;
    }

    public List<FitDevice> getCountFitDevice(){
        List<FitDevice> fitDevices = new ArrayList<>();

        Cursor c = database.query(DBHelper.MAC_ADDRESS_TABLE, null, null, null, null, null, null);
        if (c.moveToFirst()){
            do{
                int macId = c.getInt(c.getColumnIndex(DBHelper.MAC_ADDRESS_ID));
                String macAddress = c.getString(c.getColumnIndex(DBHelper.MAC_ADDRESS_MAC));
                String macName = c.getString(c.getColumnIndex(DBHelper.MAC_ADDRESS_NAME));

                FitDevice fitDevice = new FitDevice(macId, macName, macAddress);
                fitDevices.add(fitDevice);

            }while(c.moveToNext());
        }
        return fitDevices;
    }


}
